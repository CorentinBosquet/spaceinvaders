# SpaceInvaders
## Auteurs
BOSQUET Corentin
GALLIOU Quentin

VM option : --module-path /usr/lib/jvm/javafx-sdk-12.0.1/lib/ --add-modules javafx.controls,javafx.fxml

## But
Le but de ce projet est de développer un Space Invaders en Java.
Le joueur doit éliminer le plus d'aliens possible avant qu'il soit tué.
Des murs seront mis à sa disposition pour se protéger.

Le jeu se termine si le joueur n'a plus de vie ou que les aliens ont atteint le bas de l'écran.


##### Projet JAVA 2019 - IMR 1 - ENSSAT
##### :copyright: Tous droits réservés
