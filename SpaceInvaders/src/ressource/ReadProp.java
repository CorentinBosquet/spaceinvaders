package ressource;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import SpaceInvaders.Game;

public class ReadProp {
 
	/**
	 * Permet de récupérer la coniguration de la partie
	 * @param game Partie du SpaceInvaders
	 * @throws IOException
	 */
	public void init_config(Game game) throws IOException {
 
		try (InputStream input = new FileInputStream("./ressources/parametres.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out
            game.setBest_score(Integer.parseInt(prop.getProperty("db.best_score")));
            game.setLife(Integer.parseInt(prop.getProperty("db.nb_life")));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
	}
}
