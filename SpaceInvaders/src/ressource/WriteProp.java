package ressource;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import SpaceInvaders.Game;
import Vue.ConfigUI;

public class WriteProp {
	
	/**
	 * Permet de sauvegarder la configuration faite par l'utilisateur
	 */
	public static void save_game() {

        try (OutputStream output = new FileOutputStream("./src/ressources/parametres.properties")) {

            Properties prop = new Properties();

            // set the properties value
            prop.setProperty("sp.best_score", "");
            prop.setProperty("sp.nb_life", "3");
            prop.setProperty("sp.background", ConfigUI.getBackground());

            // save properties to project root folder
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
