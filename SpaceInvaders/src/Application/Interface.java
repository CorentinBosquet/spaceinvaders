package Application;

import SpaceInvaders.Collision.Collision;
import SpaceInvaders.Game;
import Vue.ConfigUI;
import Vue.GameUI;
import Vue.InformationUI;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation")
public class Interface extends Application implements Observer {
	
	private Game game = new Game();
	private GameUI ui_game;
	private VBox vb_informations = new VBox();
	
	private BorderPane fenetre = new BorderPane();


	private Service<Void> deplacement_shoot = new Service<Void>(){

		  @Override
		  protected Task<Void> createTask() {
		    return new Task<Void>(){

		     @Override
		     protected Void call() throws Exception { 
		    	ui_game.getBullet().setX(ui_game.getSpaceship().getX()+10);
		    	ui_game.getBullet().setY(ui_game.getSpaceship().getY());
		    	ui_game.getBullet().draw();
		    	while(ui_game.getBullet().getY() >= 0 /* TODO ou !collision*/) {
					ui_game.getBullet().setY(ui_game.getBullet().getY()-5);
					try {
						Thread.sleep(5);
					}catch(InterruptedException e1) {
						e1.printStackTrace();
					}
                    /*for (Alien alien : ui_game.getListAlien()) {
                        if (Collision.getInstance().check_collision(alien, ui_game.getBullet())) {
                            alien.getAlien().setVisible(false);
                            //ui_game.getListAlien().remove(alien.getAlien());
                        }
                    }*/
					Collision.getInstance().check_collision(ui_game.getAlien(), ui_game.getBullet());
					//ui_game.getPane().getChildren().remove(ui_game.getAlien().getAlien());


		    	}
		    	ui_game.getBullet().die();
		        return null;
		      }
		    };
		  }
		};
		
		/*private Service<Void> deplacement_alien = new Service<Void>(){

			  @Override
			  protected Task<Void> createTask() {
			    return new Task<Void>(){

			     @Override
			     protected Void call() throws Exception { 
			    	Double border=0.0;
			    	// position = 5*43 
			    	Integer posX =0;
			    	Integer posY =0;
			    	Integer nbcol=5; // TODO deplacer dans la classe qui gere les niveau
			    	Integer nbligne=2;// TODO deplacer dans la classe qui gere les niveau
			    	Integer cpt=0;
			    	
			    	for (int i =0; i< nbligne;i++) {
			    		for(int j=0;j<nbcol;j++) {
			    			ui_game.getListAlien().get(cpt).getAlien().setX(posX);
			    			ui_game.getListAlien().get(cpt).getAlien().setY(posY);
			    			ui_game.getListAlien().get(cpt).draw();

			    			cpt++;
                            posX += 45;
			    		}
                        posX = 0;
                        posY += 31;
			    	}
                     boolean noBottom = true;
                     while (noBottom) {//ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getY() <= 500.0 )}// TODO ou !collision
                         //System.out.println("dernier :"+ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getX());
                         if (ui_game.getListAlien().get(ui_game.getListAlien().size() - 1).getAlien().getX() == 255.0) {
                             //System.out.println("dernier alien a x = 250");
							border= 0.0;
                             while (ui_game.getListAlien().get(0).getAlien().getX() > border )}// TODO ou !collision
                                 //	System.out.println("border : "+border+" X: "+ui_game.getListAlien().get(0).getAlien().getX()+" Y : "+ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getY());
                                 //ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().setX(ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getX()-25);
                                 for (Alien alien : ui_game.getListAlien()) {
                                     alien.getAlien().setX(alien.getX() - 25);
                                 }
								try {
									Thread.sleep(500);
								}catch(InterruptedException e1) {
									e1.printStackTrace();
								}
					    	}
						}else {
							border=250.0;
							while(ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getX()< border)} //TODO ou !collision
                                //	System.out.println("border : "+border+" X: "+ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getX()+" Y : "+ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getY());
                                //ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().setX(ui_game.getListAlien().get(ui_game.getListAlien().size()-1).getAlien().getX()+25);
                                for (Alien alien : ui_game.getListAlien()) {
                                    alien.getAlien().setX(alien.getX() + 25);
                                }
								try {
									Thread.sleep(500);
								}catch(InterruptedException e1) {
									e1.printStackTrace();
								}
					    	}
						}
						for (Alien alien: ui_game.getListAlien()) {
							alien.getAlien().setY(alien.getY()+10);
						}
						try {
							Thread.sleep(500);
						}catch(InterruptedException e1) {
							e1.printStackTrace();
						}
                         for (Alien alien : ui_game.getListAlien()) {
                             if (alien.getAlien().getY() > 411) {
                                 noBottom = false;
                             }
                             ;
                         }
		
			    	}
			    	for (Alien alien: ui_game.getListAlien()) {
						alien.die();
					}
			        return null;
			      }
			    };
			  }
			};*/

	/*private Service<Void> deplacement_alien = new Service<Void>() {

		@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {

				@Override
				protected Void call() throws Exception {
					Double border = 0.0;
					ui_game.getAlien().getAlien().setX(0);
					ui_game.getAlien().getAlien().setY(0);
					ui_game.getAlien().draw();
					while (ui_game.getAlien().getY() <= 500.0 ){// TODO ou !collision
						ui_game.getAlien().getAlien().setY(ui_game.getAlien().getY() + 10);
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
						if (ui_game.getAlien().getX() == 250.0) {
							border = 0.0;
							while (ui_game.getAlien().getX() > border){ // TODO ou !collision
								System.out.println("border : " + border + " X: " + ui_game.getAlien().getX() + " Y : " + ui_game.getAlien().getY());
								ui_game.getAlien().getAlien().setX(ui_game.getAlien().getX() - 25);
								try {
									Thread.sleep(500);
								} catch (InterruptedException e1) {
									e1.printStackTrace();
								}
							}
						} else {
							border = 250.0;
							while (ui_game.getAlien().getX() < border){ // TODO ou !collision
								System.out.println("border : " + border + " X: " + ui_game.getAlien().getX() + " Y : " + ui_game.getAlien().getY());
								ui_game.getAlien().getAlien().setX(ui_game.getAlien().getX() + 25);
								try {
									Thread.sleep(500);
								} catch (InterruptedException e1) {
									e1.printStackTrace();
								}
							}
						}

					}
					ui_game.getAlien().die();
					return null;
				}
			};
		}
	};*/



		
	/**
	 * Permet d'afficher l'interface graphique.
	 * @param primaryStage
	 * @throws IOException
	 */
	public void start(Stage primaryStage) throws IOException {
		
		//init_config(game);

		//PARTIE MENU
		MenuBar menuBar = new MenuBar();
	    menuBar.prefWidthProperty().bind(primaryStage.widthProperty());

		Menu gameMenu = new Menu("Partie");
	    MenuItem newGameItem = new MenuItem("Nouvelle partie");
	    MenuItem pauseGameItem = new MenuItem("Pause");
	    MenuItem quitGameItem = new MenuItem("Quitter");

		gameMenu.getItems().addAll(newGameItem, pauseGameItem, quitGameItem);

		Menu helpMenu = new Menu("Aide");
		menuBar.getMenus().addAll(gameMenu, helpMenu);

		Button btn_partie = new Button("Nouvelle partie");
		Button btn_config = new Button("Configuration");
		
		VBox btn_liste = new VBox();
		btn_liste.getChildren().addAll(btn_partie, btn_config);
		
		btn_partie.setOnAction(e ->{
			ui_game = new GameUI();
			init_game();
			fenetre.setLeft(ui_game.getPane());
		});
		
		btn_config.setOnAction(e ->{
			fenetre.setLeft(ConfigUI.page_config());
		});

		// PARTIE DROITE
		vb_informations = new InformationUI(game).getInformations();
		vb_informations.setPrefSize(150, 500);
		vb_informations.getStyleClass().add("vbox");

		//fenetre.setLeft(ui_game.getPane());
		fenetre.setLeft(btn_liste);
		fenetre.setRight(vb_informations);
	    fenetre.setTop(menuBar);

		quitGameItem.setOnAction(actionEvent -> Platform.exit());
		newGameItem.setOnAction(actionEvent -> init_game());
		
		Scene scene = new Scene(fenetre, 450, 500);
		
		scene.setOnKeyPressed(e -> {
			switch (e.getCode()) {
			case LEFT:
				if(ui_game.getSpaceship().getPosX() > 10) {
					ui_game.getSpaceship().moveLeft();
				}
				break;
			case RIGHT:
				if(ui_game.getSpaceship().getPosX() < 250)
					ui_game.getSpaceship().moveRight();
				break;
			case P:
				game.setPause(!game.getPause());
				if(game.getPause()) {
					System.out.println("pause");
					//TODO mettre en pause les tirs
					//TODO mettre en pause des aliens
					//TODO ne plus permettre de bouger le spaceship
					//TODO flouter écran + msg pause ?
				} else {
					System.out.println("fin pause");
					//TODO reprise normale du jeu
				}
				break;
			case SPACE:
				deplacement_shoot.start();
				deplacement_shoot.setOnSucceeded(e2 -> {
					ui_game.getBullet().setVisible(false);
					deplacement_shoot.reset();
				});
				deplacement_shoot.setOnSucceeded(e2 -> {
					deplacement_shoot.reset();
				});
				break;
			case A:
				//deplacement_alien.start();
				break;
			default:
				break;
			}
		});
		
		//FIXME Path not found
		scene.getStylesheets().add("./ressources/style.css");
		
        primaryStage.setTitle("Space Invaders");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
	
	@Override
	public void update(Observable arg0, Object arg1) {
		Game game = (Game) arg1;
		vb_informations = new InformationUI(game).getInformations();
		vb_informations.getStyleClass().add("vbox");
		fenetre.setRight(vb_informations);
	}
	
	public void init_game() {
		game.addView(this);
		ui_game.initPos();
	}
	
	public static void main(String[] args) {
        launch(args);
    }
}
