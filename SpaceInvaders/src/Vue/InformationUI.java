package Vue;

import SpaceInvaders.Game;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class InformationUI {
	
	private VBox ecran_info = new VBox();
	
	private Label lbl_score = new Label("SCORE");
	private Label lbl_best_score = new Label("HIGH SCORE");
	private Label lbl_life = new Label("LIFES  3");
	
	// Constructeur
	public InformationUI(Game game) {
		lbl_score.setText("SCORE  " + String.valueOf(game.getScore()));
		lbl_best_score.setText("BEST SCORE  " + String.valueOf(game.getBest_score()));
		
		if(game.getLife() > 0) {
			lbl_life.setText("LIFES  " + String.valueOf(game.getLife()));
		} else {
			lbl_life.setText("LIFE  " + String.valueOf(game.getLife()));
		}
		
		ecran_info.setPadding(new Insets(10,10,10,10));
		ecran_info.setSpacing(20);
		ecran_info.getChildren().addAll(lbl_score, lbl_best_score, lbl_life);
		ecran_info.setStyle("-fx-border-style: solid;\n" + 
				"    -fx-border-width: 2px;\n" + 
				"    -fx-border-color: blue;");
	}
	
	/**
	 * Permet de récupérer le HBox avec les nouvelles données
	 * @return ecran_info
	 */
	public VBox getInformations(){
		return this.ecran_info;
	}

}
