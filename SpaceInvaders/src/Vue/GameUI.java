package Vue;

import SpaceInvaders.Deplacement.Alien;
import SpaceInvaders.Deplacement.Bullet;
import SpaceInvaders.Deplacement.Spaceship;
import SpaceInvaders.Deplacement.Sprite;
import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.scene.layout.Pane;

import java.util.List;
import java.util.stream.Collectors;


public class GameUI {
    private Pane pane;
    private Spaceship spaceship;
    private Bullet bullet;
    //private List<Alien> listAlien = new ArrayList<>();
    private Alien alien;
    private double t = 0;

    public GameUI() {
        this.pane = new Pane();
        this.spaceship = new Spaceship("./ressources/ship.gif", 150, 200, "spaceship");
        this.bullet = new Bullet("./ressources/shot.gif", 0, 0, "bullet");
        this.alien = new Alien("./ressources/alien.gif", 0, 0, "alien");


		/*for (int i = 0; i < 10; i++) {// TODO nb alien du niveau en variable
			this.listAlien.add(i, new Alien());
		}*/

        //this.pane = new Pane(spaceship.getSpaceship(), bullet.getBullet(),alien.getAlien());
        //this.pane.getChildren().addAll(spaceship.getSpaceship(), bullet.getBullet(), alien.getAlien());


		/*for(int i =0;i<10;i++) {// TODO nb alien du niveau en variable
			this.pane.getChildren().addAll(this.listAlien.get(i).getAlien());

		}*/
        //pane.getChildren().add(spaceship);



    }

    private void shoot(Sprite s) { // TODO abstract class Sprite
        Bullet b = new Bullet("./ressources/shot.gif", (int) s.getTranslateX() + 20, (int) s.getTranslateY() + 0, "bullet");
        pane.getChildren().add(b);
    }

    public void nextLevel() {
        for (int i = 0; i < 5; i++) {
            Alien a = new Alien("./ressources/alien.gif", 20 + i * 43, 0, "alien");
            this.pane.getChildren().add(a);
        }
    }

    private List<Sprite> sprites() {
        return pane.getChildren().stream().map(n -> (Sprite) n).collect(Collectors.toList());
    }

    private void update() {
        t += 0.016;

        sprites().forEach(s -> {
            switch (s.getType()) {
                case "alienBullet":
                    s.moveDown();
                    if (s.getBoundsInParent().intersects(spaceship.getBoundsInParent())) {
                        spaceship.setDead(true);
                        s.setDead(true);
                    }
                case "spaceshipBullet":
                    s.moveUp();
                    sprites().stream().filter(e -> e.getType().equals("alien")).forEach(alien -> {
                        if (s.getBoundsInParent().intersects(alien.getBoundsInParent())) {
                            alien.setDead(true);
                            s.setDead(true);
                        }
                    });
                    break;
                case "alien":
                    if (t > 2) {
                        if (Math.random() < 0.3) {
                            shoot(s);
                        }
                    }

                    break;
            }
        });
        pane.getChildren().removeIf(n -> {
            Sprite s = (Sprite) n;
            return s.isDead();
        });

        if (t > 2) {
            t = 0;
        }
    }

    private ObservableList sprites1() {
        System.out.println(pane.getChildren().stream().map(n -> (Sprite) n).collect(Collectors.toList()));
        ObservableList childs = pane.getChildren();
        //System.out.println(childs);
        //return pane.getChildren().stream().map(n -> (Sprite)n).collect(Collectors.toList());
        return childs;

    }

    private void update1() {
        t += 0.016;
        List<Sprite> childs = pane.getChildren().stream().map(n -> (Sprite) n).collect(Collectors.toList());
        //ObservableList childs = pane.getChildren();
        for (int i = 0; i < childs.size(); i++) {
            Sprite s = (Sprite) childs.get(i);
            System.out.println(s.getType());

            switch (s.getType()) {
                case "alienBullet":
                    s.moveDown();
                    if (s.getBoundsInParent().intersects(spaceship.getBoundsInParent())) {
                        spaceship.setDead(true);
                        s.setDead(true);
                    }
                case "spaceshipBullet":
                    s.moveUp();
                    childs.stream().filter(e -> e.getType().equals("alien")).forEach(alien -> {
                        if (s.getBoundsInParent().intersects(alien.getBoundsInParent())) {
                            alien.setDead(true);
                            s.setDead(true);
                        }
                    });
                    break;
                case "alien":
                    if (t > 2) {
                        if (Math.random() < 0.3) {
                            shoot(s);
                        }
                    }

                    break;
            }

        }
        /*childs.forEach((Sprite)s -> {
            System.out.println(s.getType());

        });*/

        pane.getChildren().removeIf(n -> {
            Sprite s = (Sprite) n;
            return s.isDead();
        });
        if (t > 2) {
            t = 0;
        }
    }


    public void initPos() {
        this.pane.setPrefSize(300, 500);
        this.pane.setStyle("-fx-border-style: solid;\n" +
                "    -fx-border-width: 2px;\n" +
                "    -fx-border-color: black;");
        this.pane.getChildren().add(spaceship);


        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                update();
            }
        };

        timer.start();
        nextLevel();


        /*this.spaceship.getSpaceship().setX(130);
        this.spaceship.getSpaceship().setY(435);

        this.bullet.getBullet().setVisible(false);
        this.alien.getAlien().setVisible(true);*/
		/*for(int i =0;i<10;i++) {// TODO nb alien du niveau en variable
			this.listAlien.get(i).getAlien().setVisible(true);
		}*/
    }

    public Pane getPane() {
        return this.pane;
    }

    public Spaceship getSpaceship() {
        return this.spaceship;
    }

    public Bullet getBullet() {
        return this.bullet;
    }

    /*public List<Alien> getListAlien() {
        return this.listAlien;
    }

    public Alien getAlien(Integer i) {
        return this.listAlien.get(i);
    }*/
    public Alien getAlien() {
        return this.alien;
    }

    public void removeAlien(Alien a) {
        System.out.println("tentative rm alien du pane");
        this.pane.getChildren().remove(a);
    }
}
