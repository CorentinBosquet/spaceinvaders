package Vue;

import java.io.File;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ressource.WriteProp;

public class ConfigUI {
	
	static public File background = new File("");
	
	static public GridPane page_config() {
		GridPane root = new GridPane();
		
		Label lb_nb_alien = new Label("Nombre d'alien");
		Label lb_vi_alien = new Label("Vitesse des aliens");
		Label lb_mod_img_fond = new Label("Fond d'écran");
		Label lb_int_tir = new Label("Intervale des tirs");
		
		Button btn_ok = new Button("Valider");
		
		Slider nb_alien = new Slider();
		nb_alien.setMin(5);
		nb_alien.setMax(25);
		nb_alien.setBlockIncrement(5);
		nb_alien.setValue(5);
		
		Slider vi_alien = new Slider();
		vi_alien.setMin(5);
		vi_alien.setMax(25);
		vi_alien.setBlockIncrement(5);
		vi_alien.setValue(5);
		
		Slider int_tir  = new Slider();
		int_tir.setMin(5);
		int_tir.setMax(25);
		int_tir.setBlockIncrement(5);
		int_tir.setValue(5);
		
		FileChooser img_fond = new FileChooser();
		Button btn_img_but = new Button("fichier");
		
		root.add(lb_nb_alien, 0, 0);
		root.add(nb_alien, 1, 0);
		
		root.add(lb_vi_alien, 0, 1);
		root.add(vi_alien, 1, 1);
		
		root.add(lb_int_tir, 0, 2);
		root.add(int_tir, 1, 2);
		
		root.add(lb_mod_img_fond, 0, 3);
		root.add(btn_img_but, 1, 3);
		
		root.add(btn_ok, 0, 4, 2, 1);
		btn_ok.setAlignment(Pos.CENTER);
		
		root.setAlignment(Pos.TOP_CENTER);
		root.setGridLinesVisible(true);
		
		btn_img_but.setOnAction(e -> {
            background = img_fond.showOpenDialog(new Stage());
        });
		
		btn_ok.setOnAction(e ->{
			double nb_a = nb_alien.getValue();
			double vi_a = vi_alien.getValue();
			double int_t = int_tir.getValue();
			
			WriteProp.save_game();
			// TODO retour menu
		});
		
        return root;
	}
	
	public static String getBackground() {
		return background.getAbsolutePath();
	}
}
