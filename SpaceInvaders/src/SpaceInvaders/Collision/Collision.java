package SpaceInvaders.Collision;

import SpaceInvaders.Deplacement.Alien;
import SpaceInvaders.Deplacement.Bullet;
import Vue.GameUI;

public final class Collision {
	
	// Instance de Collision
	private static volatile Collision collision = null;
	private GameUI ui_game = new GameUI();

	// Constructeur
	private Collision() {
		super();
	}
	
	/**
	 * Retourne une instance de Collision 
	 * @return collision 
	 */
	public static Collision getInstance() {
		if(Collision.collision == null) {
			synchronized(Collision.class) {
				if(Collision.collision == null) {
					Collision.collision = new Collision();
				}
			}
		}
		return Collision.collision;
	}
	
	/**
	 * Permet de détecter une collision
	 * @param a Alien
	 * @param b Bullet
	 * @return bollean : true si collision sinon non
	 */
	public void check_collision(Alien a, Bullet b) {

		if(a.isVisible() && b.isVisible()) {
			if (a.getX() <= b.getX() && a.getX() + 43 >= b.getX()
					&& a.getY() == b.getY()) { //FIXME 43 : largeur alien
				System.out.println("touché alien x : " + a.getX() + " y :" + a.getY() + " bullet x : " + b.getX() + " y : " + b.getY());
				ui_game.removeAlien(ui_game.getAlien());
			}
		}
		//System.out.println("loupé t nul !");
	}
}
