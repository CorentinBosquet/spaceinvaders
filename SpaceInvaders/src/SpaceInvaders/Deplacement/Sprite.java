package SpaceInvaders.Deplacement;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class Sprite extends ImageView {
    private boolean dead = false;
    private String type;

    public Sprite(String path, int x, int y, String type) {
        super(new Image(path));
        //this.dead = false;
        this.type = type;
        setTranslateX(x);
        setTranslateX(y);
    }

    public void moveLeft() {
        setTranslateX(getTranslateX() - 5);
    }

    public void moveRight() {
        setTranslateX(getTranslateX() + 5);
    }

    public void moveUp() {
        setTranslateY(getTranslateY() + 5);
    }

    public void moveDown() {
        setTranslateY(getTranslateY() - 5);
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public String getType() {
        return type;
    }

}
