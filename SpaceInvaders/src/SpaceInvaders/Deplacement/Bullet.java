package SpaceInvaders.Deplacement;

public class Bullet extends Sprite {

	private boolean dead = false;
	private String type;


	public Bullet(String path, int x, int y, String type) {
		super(path, x, y, type);
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	/*public String getType() {
		return type;
	}*/

	public void setType(String type) {
		this.type = type;
	}

	public void moveLeft() {
		setTranslateX(getTranslateX() - 5);
	}

	public void moveRight() {
		setTranslateX(getTranslateX() + 5);
	}

	public void moveUp() {
		setTranslateY(getTranslateY() + 5);
	}

	public void moveDown() {
		setTranslateY(getTranslateY() - 5);
	}


	/**
	 * Retourne l'ImageView d'un tir
	 * @return bullet
	 */
	/*public ImageView getBullet() {
		return this.bullet;
	}*/
	
	/**
	 * Retourne la position de l'ImageView sur l'axe horizontal
	 * @return la position de l'ImageView
	 */
	/*public double getX() {
		return this.getX();
	}*/
	
	/**
	 * Retourne la position de l'ImageView sur l'axe verticale
	 * @return la position de l'ImageView
	 */
/*	public double getY() {
		return this.getY();
	}*/
	
	/**
	 * Permet d'afficher à l'écran l'image d'un tir
	 */
	public void draw() {
		this.setVisible(true);
	}
	
	/**
	 * Permet de cacher à l'écran l'image d'un tir
	 */
	public void die() {
		this.setVisible(false);
	}
	
}
