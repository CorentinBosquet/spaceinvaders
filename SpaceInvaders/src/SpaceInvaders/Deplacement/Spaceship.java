package SpaceInvaders.Deplacement;

public class Spaceship extends Sprite {
	
	private boolean dead = false;
	private String type;
	
	// Constructeur
	public Spaceship(String path, int x, int y, String type) {
		super(path, x, y, type);
	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	/*public String getType() {
		return type;
	}*/



	public void moveLeft() {
		setTranslateX(getTranslateX() - 5);
	}

	public void moveRight() {
		setTranslateX(getTranslateX() + 5);
	}

	public void moveUp() {
		setTranslateY(getTranslateY() + 5);
	}

	public void moveDown() {
		setTranslateY(getTranslateY() - 5);
	}
	/**
	 * Permet de récupérer l'objet ImageView du vaisseau
	 * @return L'image du vaisseau
	 */
	/*public ImageView getSpaceship() {
		return this.spaceship;
	}*/
	
	/**
	 * Récupère la position du vaisseau sur l'axe horizontal
	 * @return position horizontal du vaisseau
	 */
	public double getPosX() {
		return this.getTranslateX();
	}
	
	/**
	 * Récupère la position du vaisseau sur l'axe vertical
	 * @return position vertical du vaisseau
	 */
	public double getPosY() {
		return this.getTranslateY();
	}
}
