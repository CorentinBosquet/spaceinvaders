package SpaceInvaders.Deplacement;

public class Alien extends Sprite {
	private boolean dead = false;
	private String type;

	public Alien(String path, int x, int y, String type) {
		super(path, x, y, type);

	}

	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}

	public void moveLeft() {
		setTranslateX(getTranslateX() - 5);
	}

	public void moveRight() {
		setTranslateX(getTranslateX() + 5);
	}

	public void moveUp() {
		setTranslateY(getTranslateY() + 5);
	}

	public void moveDown() {
		setTranslateY(getTranslateY() - 5);
	}

	
	/*public ImageView getAlien() {
		return this.alien;
	}*/

	/*public double getX() {
		return this.alien.getX();
	}
	
	public double getY() {
		return this.alien.getY();
	}
	*/
	/*public void draw() {
		this.alien.setVisible(true);
	}
	
	public void die() {
		this.alien.setVisible(false);
	}*/
	
}
