package SpaceInvaders;

import java.util.Observable;
import java.util.Observer;

@SuppressWarnings("deprecation")
public class Game extends Observable {
	
	private int score;
	private int best_score;
	private int life;
	private Boolean pause;
	
	// Constructeur
	public Game() {
		super();
		this.pause = false;
	}

	/**
	 * Permet de récupérer le score de la partie
	 * @return score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Permet de modifier le score de la partie
	 * @param score
	 */
	public void setScore(int score) {
		this.score = score;
		notifyView();
	}

	/**
	 * Permet de récupérer le meilleur score des parties
	 * @return best_score
	 */
	public int getBest_score() {
		return best_score;
	}

	/**
	 * Permet de modifier le meilleur score de la partie
	 * @param best_score
	 */
	public void setBest_score(int best_score) {
		this.best_score = best_score;
		notifyView();
	}

	/**
	 * Permet de récupérer les vies restantes de la partie
	 * @return life
	 */
	public int getLife() {
		return life;
	}

	/**
	 * Permet de modifier le nombre de vie de la partie
	 * @param life
	 */
	public void setLife(int life) {
		this.life = life;
		notifyView();
		if(this.life == 0) {
			game_over();
		}
	}
	
	/**
	 * Permet de récupérer l'état de la partie (pause ou non)
	 * @return pause
	 */
	public Boolean getPause() {
		return this.pause;
	}
	
	/**
	 * Permet d'activer ou désactiver l'état de pause de la partie
	 * @param pause
	 */
	public void setPause(Boolean pause) {
		this.pause = pause;
	}

	/**
	 * Permet d'ajouter un observer à la partie
	 * @param view
	 */
	public void addView(Observer view) {
		addObserver(view);
	}
	
	/**
	 * Permet d'enlever un observer à la partie
	 * @param view
	 */
	public void deleteView(Observer view) {
		deleteObserver(view);
	}
	
	/**
	 * Permet de notifier tous les observer de la partie
	 */
	public void notifyView() {
		setChanged();
		notifyObservers(this);
	}
	
	/**
	 * Permet de couper la partie lorsque le joueur a perdu
	 */
	public void game_over() {
		// TODO stop boucle jeu
		if(this.score > this.best_score) {
			//TODO enregistrement du nouveau best score
		}
		//TODO afficher bilan partie
	}
	
}
